import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/Book';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.sass']
})
export class BooksComponent implements OnInit {
  books: Book[];
  constructor(private bookService: BooksService) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe( books => {
      this.books = books;
      console.log(books[0]);
    });
  }
}
